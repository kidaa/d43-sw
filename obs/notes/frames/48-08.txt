===== The Lamb of God [48-08] Mwana Kondoo wa Mungu =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-48-08.jpg?nolink&}}

When **God** told **Abraham** to offer his **son**, **Isaac**, as a **sacrifice**, God provided a **lamb** for the sacrifice **instead of his son**, **Isaac**. We all **deserve to die** for our **sins**! But God provided **Jesus**, the **Lamb of God**, as a sacrifice **to die in our place**.

Wakati **Mungu ** alimwambia **Ibrahimu ** amtoe **mwanawe**, **Isaka**, kama **dhabihu**, Mungu alitoa **mwana kondoo** kwa ajili ya dhabihu **badala ya mwanawe, Isaka**. Sisi zote **tunastahili kufa ** kwa ajili ya **dhambi ** zetu! Lakini Mungu alimtoa **Yesu**,** Mwana Kondoo wa Mungu** kama dhabihu **afe mahali petu**.
===== Important Terms (Maneno ya Muhimu): =====

  * **[[:sw:obs:notes:key-terms:god|God (Mungu)]]**
  * **[[:sw:obs:notes:key-terms:abraham|Abraham (Ibrahimu)]]**
  * **[[:sw:obs:notes:key-terms:son|son (mwana)]]**
  * **[[:sw:obs:notes:key-terms:isaac|Isaac (Isaka)]]**
  * **[[:sw:obs:notes:key-terms:sacrifice|sacrifice (dhabihu)]]**
  * **[[:sw:obs:notes:key-terms:lamb|lamb (mwana kondoo)]]**
  * **[[:sw:obs:notes:key-terms:death|die (afe)]]**
  * **[[:sw:obs:notes:key-terms:sin|sins (dhambi)]]**
  * **[[:sw:obs:notes:key-terms:jesus|Jesus (Yesu)]]**
  * **[[:sw:obs:notes:key-terms:lamb|Lamb of God (Mwana Kondoo wa Mungu)]]**
===== Translation Notes (Maelezo ya Tafsiri): =====

  * **instead of his son, Isaac**  - This can also be translated as, "in the place of his son, Isaac" or, "in his son Isaac's place" or, "so that he would not have to offer his son Isaac as a sacrifice."
  * **badala ya mwanawe, ****Isaka**  - Hii inaweza pia kutafsiriwa kama, "mahali pa mwanawe, Isaka" ama, katika mahali pa Isaka" ama, ili asimtoe mwanawe Isaka kama dhabihu."
  * **deserve to die**  - That is, "should die."
  * **tunastahili kufa**  - Hiyo ni, "tunapaswa kufa."
  * **to die in our place**  - This can also be translated as, "to die in the place of each of us" or, "so that he would not have to kill us."
  * **afe mahali petu**  - Hii inaweza pia kutafsiriwa kama, "kufa mahali pa kila mmoja wetu." ama, "ili asiweze kutuua."
**[[:sw:obs:notes:frames:48-07|<<]] | [[:sw:obs:notes:48|Up]] | [[:sw:obs:notes:frames:48-09|>>]]**
